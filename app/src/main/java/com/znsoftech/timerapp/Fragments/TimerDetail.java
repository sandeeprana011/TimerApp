package com.znsoftech.timerapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.znsoftech.timerapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimerDetail extends Fragment {


   public TimerDetail() {
	  // Required empty public constructor
   }


   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
							Bundle savedInstanceState) {
	  // Inflate the layout for this fragment
	  return inflater.inflate(R.layout.fragment_timer_detail, container, false);
   }

   @Override
   public void onViewCreated(View view, Bundle savedInstanceState) {
	  super.onViewCreated(view, savedInstanceState);

   }
}
