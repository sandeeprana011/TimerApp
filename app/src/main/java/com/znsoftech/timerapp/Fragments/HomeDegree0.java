package com.znsoftech.timerapp.Fragments;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.znsoftech.timerapp.DataStructure.DataDegree0;
import com.znsoftech.timerapp.DataStructure.Fields;
import com.znsoftech.timerapp.ErrorSystem;
import com.znsoftech.timerapp.Network.DownloadJsonContent;
import com.znsoftech.timerapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeDegree0 extends Fragment {

   public static final String HOME_DEGREE_0 = "HomeDegree0";
   private GridView gridView;
   private JSONParsingandDownloading downloading;
//   private Context context;


   public HomeDegree0() {
	  // Required empty public constructor
//	  this.context = context;
   }


   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
							Bundle savedInstanceState) {
	  // Inflate the layout for this fragment
	  return inflater.inflate(R.layout.fragment_home_degree0, container, false);
   }

   @Override
   public void onViewCreated(View view, Bundle savedInstanceState) {
	  super.onViewCreated(view, savedInstanceState);
	  gridView = (GridView) view.findViewById(R.id.gridDegree0);

	  if (DownloadJsonContent.isNetwork(getActivity().getApplicationContext())) {
		 downloading = new JSONParsingandDownloading();
		 downloading.execute(Fields
				 .URL_DATA_DEGREE_0, "");
	  } else {
//		 nonetworkavailable
	  }



//	  AdapterGridDegree0 gridAdapter=new AdapterGridDegree0();
//	  gridView.setAdapter();

   }

   @Override
   public void onDestroyView() {
	  super.onDestroyView();
	  if (downloading != null) {
		 downloading.cancel(true);
	  }

   }

   @Override
   public void onDestroy() {
	  super.onDestroy();
	  if (downloading != null) {
		 downloading.cancel(true);
	  }
   }

   /**
	* Adapter to insert data into gridview in HOme fragment
	*/
   class AdapterGridDegree0 extends BaseAdapter {


	  ArrayList<DataDegree0> arrDataDegree;

	  public AdapterGridDegree0(ArrayList<DataDegree0> arrayListDataDegree) {
		 this.arrDataDegree = arrayListDataDegree;
	  }

	  /**
	   * How many items are in the data set represented by this Adapter.
	   *
	   * @return Count of items.
	   */
	  @Override
	  public int getCount() {
		 return arrDataDegree.size();
	  }

	  @Override
	  public Object getItem(int position) {
		 return arrDataDegree.get(position);
	  }

	  @Override
	  public long getItemId(int position) {
		 return position;
	  }

	  @Override

	  public View getView(int position, View convertView, ViewGroup parent) {


		 LayoutInflater inflater;

		 inflater = LayoutInflater.from(getContext());
		 if (convertView == null) {
			convertView = inflater.inflate(R.layout.griddegree0, parent, false);
		 }

		 convertView.setMinimumHeight(convertView.getWidth());

		 DataDegree0 degree0 = this.arrDataDegree.get(position);

		 ImageView img = null;
		 TextView title = null;
		 if (degree0 != null) {
			img = (ImageView) convertView.findViewById(R.id.gridImage);
			title = (TextView) convertView.findViewById(R.id.gridTitle);
		 }

		 String urlImage = Fields.URL_IMAGE_BASE + degree0.getURLCategoryImage();


		 Glide
				 .with(getActivity().getApplicationContext())
				 .load(urlImage)
				 .centerCrop()
				 .placeholder(R.mipmap.ic_launcher)
				 .crossFade()
				 .into(img);

		 title.setText(degree0.getCategoryName());

		 return convertView;
	  }
   }

   class JSONParsingandDownloading extends AsyncTask<String, Integer, ErrorSystem> {

	  private ErrorSystem errorSystem;
	  private ArrayList<DataDegree0> dataDegree0ArrayList;

	  @Override
	  protected void onPreExecute() {
		 super.onPreExecute();
		 errorSystem = new ErrorSystem();
		 dataDegree0ArrayList = new ArrayList<>();
		 errorSystem.setHasError(false);
	  }

	  @Override
	  protected ErrorSystem doInBackground(String... params) {
		 try {
			String jsonDataString = DownloadJsonContent.downloadContent(params[0]);
			JSONObject jsonRoot = new JSONObject(jsonDataString);
			if (isOk(jsonRoot, Fields.STATUS)) {

			   if (jsonRoot.getInt(Fields.STATUS) == 200) {
				  JSONArray arrData = jsonRoot.getJSONArray(Fields.DATA);
				  if (arrData.length() > 0) {


					 for (int i = 0; i < arrData.length(); i++) {
						DataDegree0 degree0DAta = new DataDegree0();
						JSONObject object = arrData.getJSONObject(i);
						//setting category ID
						if (isOk(object, Fields.CATEGORY_ID)) {
						   degree0DAta.setCategoryID(Integer.valueOf(object.getString(Fields
								   .CATEGORY_ID)));
						} else {
						   errorSystem.setHasError(true);
						   errorSystem.setErrorString("CategoryID not found!");
						}

						if (isOk(object, Fields.CATEGORY_NAME)) {
						   degree0DAta.setCategoryName(object.getString(Fields.CATEGORY_NAME));
						} else {
						   errorSystem.setHasError(true);
						   errorSystem.setErrorString("CategoryName not found");
						}

						if (isOk(object, Fields.CATEGORY_DESC)) {
						   degree0DAta.setCategoryDesc(object.getString(Fields.CATEGORY_DESC));
						} else {
						   errorSystem.setHasError(true);
						   errorSystem.setErrorString("CategoryDesc not found");
						}


						if (isOk(object, Fields.PARENT_CATEGORY)) {
						   degree0DAta.setParentCategory(object.getString(Fields.PARENT_CATEGORY));
						} else {
						   errorSystem.setHasError(true);
						   errorSystem.setErrorString("ParentCategory not found");
						}


						if (isOk(object, Fields.CATEGORY_IMAGE)) {
						   degree0DAta.setURLCategoryImage(object.getString(Fields.CATEGORY_IMAGE));
						} else {
						   errorSystem.setHasError(true);
						   errorSystem.setErrorString("CategoryImage not found");
						}


						if (isOk(object, Fields.STATUS)) {
						   degree0DAta.setStatus(object.getString(Fields.STATUS));
						} else {
						   errorSystem.setHasError(true);
						   errorSystem.setErrorString("CategoryStatus not found");
						}

						//add datadegree0 object to an arraylist
						dataDegree0ArrayList.add(degree0DAta);
					 }

				  } else {
					 errorSystem.setHasError(true);
					 errorSystem.setErrorString("Status Success but empty Data");
				  }
			   } else {
				  errorSystem.setHasError(true);
				  errorSystem.setErrorString("Problem in Data Object format");
			   }
			} else {
			   errorSystem.setHasError(true);
			   errorSystem.setErrorString("invalid data recieved");
			}


		 } catch (IOException | JSONException e) {
			e.printStackTrace();
			errorSystem.setHasError(true);
			errorSystem.setErrorString("Exception " + e.getMessage());
		 }
		 return this.errorSystem;
	  }

	  @Override
	  protected void onPostExecute(ErrorSystem errorSystem) {
		 super.onPostExecute(errorSystem);

		 if (!this.isCancelled()) {
			if (!errorSystem.isHasError()) {
			   if (gridView != null) {
				  /**
				   * Setting the adapter
				   */
				  gridView.setAdapter(new AdapterGridDegree0(this.dataDegree0ArrayList));

				  /**
				   * Setting listener here that allows us to jump into new category for
				   * Degree1
				   */
				  gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					 @Override
					 public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						FragmentManager manager = getFragmentManager();

						ViewGroup mContainer = (ViewGroup) view.findViewById(R.id.fragmentID);
						if (mContainer != null) {
						   mContainer.removeAllViews();
						}


						Bundle bundle=new Bundle(2);
						bundle.putString(Fields.CATEGORY_ID, String.valueOf(dataDegree0ArrayList.get(position).getCategoryID()));


						HomeDegree1 degree1=new HomeDegree1();
						degree1.setArguments(bundle);

						FragmentTransaction transaction = manager.beginTransaction().replace(R.id
								.fragmentID, degree1);

						transaction.addToBackStack(HomeDegree1.HOME_DEGREE_1);
						transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim
								.slide_out_right);
						transaction.commit();

					 }
				  });

			   } else {
				  Toast.makeText(getContext(), "Gridview is null", Toast.LENGTH_LONG).show();
			   }
			} else {
			   Toast.makeText(getContext(), errorSystem.getErrorString(), Toast.LENGTH_LONG).show();
			}
		 } else {

			Log.d("Cancelled", "Async Cancelled");
		 }
	  }

	  private boolean isOk(JSONObject obj, String key) {
		 if (obj.has(key) && !obj.isNull(key)) {
			return true;
		 } else
			return false;
	  }

   }


}
