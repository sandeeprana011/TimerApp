package com.znsoftech.timerapp;

/**
 * Created by sandeeprana on 15/02/16.
 *
 */
public class ErrorSystem {
   private boolean hasError;
   private String errorString="Error : ";


   public boolean isHasError() {
	  return hasError;
   }

   public void setHasError(boolean hasError) {
	  this.hasError = hasError;
   }

   public String getErrorString() {
	  return errorString;
   }

   public void setErrorString(String errorString) {
	  this.errorString = this.errorString+"\n"+errorString;
   }
}
