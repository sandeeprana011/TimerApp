package com.znsoftech.timerapp.DataStructure;

/**
 * Created by sandeeprana on 12/02/16.
 * Class contains fields related to data degree
 */
public class DataDegree0 {
   private int CategoryID;
   private String CategoryName;
   private String CategoryDesc;
   private String ParentCategory;
   private String URLCategoryImage;
   private String Status;

   public int getCategoryID() {
	  return CategoryID;
   }

   public void setCategoryID(int categoryID) {
	  CategoryID = categoryID;
   }

   public String getCategoryName() {
	  return CategoryName;
   }

   public void setCategoryName(String categoryName) {
	  CategoryName = categoryName;
   }

   public String getCategoryDesc() {
	  return CategoryDesc;
   }

   public void setCategoryDesc(String categoryDesc) {
	  CategoryDesc = categoryDesc;
   }

   public String getParentCategory() {
	  return ParentCategory;
   }

   public void setParentCategory(String parentCategory) {
	  ParentCategory = parentCategory;
   }

   public String getURLCategoryImage() {
	  return URLCategoryImage;
   }

   public void setURLCategoryImage(String URLCategoryImage) {
	  this.URLCategoryImage = URLCategoryImage;
   }


   public String getStatus() {
	  return Status;
   }

   public void setStatus(String status) {
	  Status = status;
   }


}
