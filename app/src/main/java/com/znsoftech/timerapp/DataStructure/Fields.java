package com.znsoftech.timerapp.DataStructure;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by sandeeprana on 13/02/16.
 *
 */
public class Fields {
   public final static String STATUS = "Status";
   public final static String CATEGORY_NAME = "CategoryName";
   public final static String CATEGORY_ID = "CategoryID";
   public final static String SUB_PARENT_CATEGORY = "SubParentCategory";
   public final static String CATEGORY_IMAGE = "category_image";
   public final static String CREATED_ON = "CreatedOn";
   public final static String CREATED_BY = "CreatedBy";
   public final static String PARENT_CATEGORY = "ParentCategory";
   public final static String CATEGORY_DESC = "CategoryDesc";

   public final static String URL_BASE="http://timerapp.znsoftech.com/backend/web/index.php?r=";
   public final static String URL_DATA_DEGREE_0 =URL_BASE+"webservice/get/parent_category";
   public final static String URL_DATA_DEGREE_1
		   =URL_BASE+"webservice/get/child_category";
   public static final String DATA = "Data";

   public static final String URL_IMAGE_BASE = "http://timerapp.znsoftech.com/backend/web/";
   public static final String URL_KEY = "URL";



   public static String AREAID = "AreaID";
   public static String PHONE = "Phone";
   public static String TIMERECORDDESC = "TimeRecordDesc";
   public static String CONTACTPERSON = "ContactPerson";
   public static String WEBSITE = "Website";
   public static String WHATSAPP = "WhatsApp";
   public static String UPLOADIMAGE = "UploadImage";
   public static String TIMERECORDID = "TimeRecordID";
   public static String MODIFIEDON = "ModifiedOn";
   public static String OPENINGTIME = "OpeningTime";
   public static String TIMERECORDNAME = "TimeRecordName";
   public static String TIMERECORDDATA = "TimeRecordData";
   public static String EMAIL = "Email";
   public static String CATEGORYID = "CategoryID";
   public static String ADDRESS = "Address";
   public static String CREATEDON = "CreatedOn";
   public static String CREATEDBY = "CreatedBy";
   public static String MOBILE = "Mobile";
   public static String LATITUDE = "Latitude";
   public static String LONGITUDE = "Longitude";
   public static String CLOSINGTIME = "ClosingTime";









   public static String keyManipulator() {

	  String strig = "{\"Status\":200,\"StatusMessage\":\"Record Fetched\",\"Data\":{\"TimeRecordID\":\"2\",\"CategoryID\":\"2\",\"TimeRecordName\":\"Al Habib\",\"TimeRecordData\":\"na\",\"TimeRecordDesc\":\"na\",\"UploadImage\":\"uploads\\/104905924.jpg\",\"OpeningTime\":\"15:35:00\",\"ClosingTime\":\"15:35:00\",\"Address\":\"Plot No-4, Upper Basement, Pocket-2, Jasola Vihar, New Delhi-110025\",\"ContactPerson\":\"Mosque Al Abu Baqr\",\"Phone\":\"9871705804\",\"Mobile\":\"9871705804\",\"Email\":\"khan.sabir1992@gmail.comm\",\"Website\":\"www.znsoftech.com\",\"WhatsApp\":\"9871705804\",\"Longitude\":\"77.2931985\",\"Latitude\":\"28.5567398\",\"AreaID\":\"2\",\"CreatedBy\":\"2\",\"CreatedOn\":\"0000-00-00 00:00:00\",\"ModifiedOn\":\"2016-02-08 00:00:00\"}}";


	  String code = "";
	  JSONObject object = null;
	  try {
		 object = new JSONObject(strig);
//		 JSONArray array = null;

//		 array = object.getJSONArray("Data");
		 JSONObject object1 = null;

		 object1 = object.getJSONObject("Data");

		 Iterator<String> array1 = object1.keys();
		 while (array1.hasNext()) {
			String name = array1.next().toString();
			code = code.concat("public static String " + name.toUpperCase() + " = \"" + name + "\";" +
					"\n");
		 }
		 Log.e("Fields-------- \n\n\n\n\n\n", code);

	  } catch (JSONException e) {
		 Log.e("JsonExcep", e.getMessage());
	  }

	  return code;
   }
}
