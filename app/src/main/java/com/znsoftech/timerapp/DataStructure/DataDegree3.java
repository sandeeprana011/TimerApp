package com.znsoftech.timerapp.DataStructure;

/**
 * Created by sandeeprana on 16/02/16.
 * Encapsulated Fields
 */
public class DataDegree3 {
   private String areaID;
   private String phone;
   private String timeRecordDesc;
   private String contactPerson;
   private String website;
   private String whatsapp;
   private String uploadImage;
   private String timerRecordID;
   private String modifiedOn;
   private String openingTime;
   private String timerRecordName;
   private String timerRecordData;
   private String email;
   private String categoryID;
   private String address;
   private String createdOn;
   private String createdBy;
   private String mobile;
   private String latitude;
   private String longitude;
   private String closingTime;

   public String getAreaID() {
	  return areaID;
   }

   public void setAreaID(String areaID) {
	  this.areaID = areaID;
   }

   public String getPhone() {
	  return phone;
   }

   public void setPhone(String phone) {
	  this.phone = phone;
   }

   public String getTimeRecordDesc() {
	  return timeRecordDesc;
   }

   public void setTimeRecordDesc(String timeRecordDesc) {
	  this.timeRecordDesc = timeRecordDesc;
   }

   public String getContactPerson() {
	  return contactPerson;
   }

   public void setContactPerson(String contactPerson) {
	  this.contactPerson = contactPerson;
   }

   public String getWebsite() {
	  return website;
   }

   public void setWebsite(String website) {
	  this.website = website;
   }

   public String getWhatsapp() {
	  return whatsapp;
   }

   public void setWhatsapp(String whatsapp) {
	  this.whatsapp = whatsapp;
   }

   public String getUploadImage() {
	  return uploadImage;
   }

   public void setUploadImage(String uploadImage) {
	  this.uploadImage = uploadImage;
   }

   public String getTimerRecordID() {
	  return timerRecordID;
   }

   public void setTimerRecordID(String timerRecordID) {
	  this.timerRecordID = timerRecordID;
   }

   public String getModifiedOn() {
	  return modifiedOn;
   }

   public void setModifiedOn(String modifiedOn) {
	  this.modifiedOn = modifiedOn;
   }

   public String getOpeningTime() {
	  return openingTime;
   }

   public void setOpeningTime(String openingTime) {
	  this.openingTime = openingTime;
   }

   public String getTimerRecordName() {
	  return timerRecordName;
   }

   public void setTimerRecordName(String timerRecordName) {
	  this.timerRecordName = timerRecordName;
   }

   public String getTimerRecordData() {
	  return timerRecordData;
   }

   public void setTimerRecordData(String timerRecordData) {
	  this.timerRecordData = timerRecordData;
   }

   public String getEmail() {
	  return email;
   }

   public void setEmail(String email) {
	  this.email = email;
   }

   public String getCategoryID() {
	  return categoryID;
   }

   public void setCategoryID(String categoryID) {
	  this.categoryID = categoryID;
   }

   public String getAddress() {
	  return address;
   }

   public void setAddress(String address) {
	  this.address = address;
   }

   public String getCreatedOn() {
	  return createdOn;
   }

   public void setCreatedOn(String createdOn) {
	  this.createdOn = createdOn;
   }

   public String getCreatedBy() {
	  return createdBy;
   }

   public void setCreatedBy(String createdBy) {
	  this.createdBy = createdBy;
   }

   public String getMobile() {
	  return mobile;
   }

   public void setMobile(String mobile) {
	  this.mobile = mobile;
   }

   public String getLatitude() {
	  return latitude;
   }

   public void setLatitude(String latitude) {
	  this.latitude = latitude;
   }

   public String getLongitude() {
	  return longitude;
   }

   public void setLongitude(String longitude) {
	  this.longitude = longitude;
   }

   public String getClosingTime() {
	  return closingTime;
   }

   public void setClosingTime(String closingTime) {
	  this.closingTime = closingTime;
   }
}
