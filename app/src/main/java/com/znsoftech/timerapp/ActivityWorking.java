package com.znsoftech.timerapp;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.znsoftech.timerapp.DataStructure.Fields;
import com.znsoftech.timerapp.Fragments.HomeDegree0;

import java.lang.reflect.Field;

public class ActivityWorking extends AppCompatActivity {

   @Override
   protected void onCreate(Bundle savedInstanceState) {
	  super.onCreate(savedInstanceState);
	  setContentView(R.layout.activity_working);


//	  String data2="{\"Status\":200,\"StatusMessage\":\"Record Fetched\",\"Data\":{\"TimeRecordID\":\"2\",\"CategoryID\":\"2\",\"TimeRecordName\":\"Al Habib\",\"TimeRecordData\":\"na\",\"TimeRecordDesc\":\"na\",\"UploadImage\":\"uploads\\/104905924.jpg\",\"OpeningTime\":\"15:35:00\",\"ClosingTime\":\"15:35:00\",\"Address\":\"Plot No-4, Upper Basement, Pocket-2, Jasola Vihar, New Delhi-110025\",\"ContactPerson\":\"Mosque Al Abu Baqr\",\"Phone\":\"9871705804\",\"Mobile\":\"9871705804\",\"Email\":\"khan.sabir1992@gmail.comm\",\"Website\":\"www.znsoftech.com\",\"WhatsApp\":\"9871705804\",\"Longitude\":\"77.2931985\",\"Latitude\":\"28.5567398\",\"AreaID\":\"2\",\"CreatedBy\":\"2\",\"CreatedOn\":\"0000-00-00 00:00:00\",\"ModifiedOn\":\"2016-02-08 00:00:00\"}}";
//
//
//	  ;
//	  Log.e("data2",Fields.keyManipulator());

	  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
	  fab.setOnClickListener(new View.OnClickListener() {
		 @Override
		 public void onClick(View view) {
			FragmentManager manager=getSupportFragmentManager();

			ViewGroup mContainer = (ViewGroup) findViewById(R.id.fragmentID);
			mContainer.removeAllViews();
			FragmentTransaction transaction=manager.beginTransaction().replace(R.id.fragmentID,
					new HomeDegree0());
//			transaction.addToBackStack(HomeDegree0.HOME_DEGREE_0);
			transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim
					.slide_out_right);
			transaction.commit();
			view.setVisibility(View.GONE);
		 }

	  });


   }

}
